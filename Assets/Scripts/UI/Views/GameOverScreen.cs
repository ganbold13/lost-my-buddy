using UnityEngine;
using UnityEngine.UI;
using LostMyBuddy;

public class GameOverScreen : View
{
    [SerializeField] Button replayButton, backButton;

    public override void Init()
    {
        replayButton.onClick.AddListener(() =>
        {
            GameManager.Instance.Replay();
        });

        backButton.onClick.AddListener(() =>
        {
            Player.Instance.Restart();
            GridManager.Instance.Clear();
            UIManager.Instance.Show<LevelSelectMenu>();
        });
    }
}
