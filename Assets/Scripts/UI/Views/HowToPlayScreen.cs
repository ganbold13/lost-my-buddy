using UnityEngine;
using UnityEngine.UI;
using LostMyBuddy;

public class HowToPlayScreen : View
{
    [SerializeField] Button backButton;
    public override void Init()
    {
        base.Init();
        backButton.onClick.AddListener(() =>
        {
            UIManager.Instance.Show<MainMenu>();
            UIManager.Instance.Hide<HowToPlayScreen>();
        });
    }
}
