using UnityEngine;
using UnityEngine.UI;
using LostMyBuddy;

public class PauseMenu : View
{
    [SerializeField] Button resumeButton, restartButton, backToMenuButton;

    public override void Init()
    {
        resumeButton.onClick.AddListener(() =>
        {
            GameManager.Instance.Resume();
        });

        restartButton.onClick.AddListener(() =>
        {
            GameManager.Instance.Replay();
        });

        backToMenuButton.onClick.AddListener(() =>
        {
            Player.Instance.Restart();
            GridManager.Instance.Clear();
            UIManager.Instance.Show<LevelSelectMenu>();
        });
    }
}

