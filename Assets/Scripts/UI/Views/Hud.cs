using UnityEngine;
using DG.Tweening;
using TMPro;
using LostMyBuddy;

public class Hud : View
{
    [SerializeField] Transform tutorialHand;
    [SerializeField] TMP_Text moveCountText;

    [Space]
    [Header("Money Field")]
    [SerializeField] Transform moneyBG;
    [SerializeField] TMP_Text moneyText;

    public override void Init()
    {
        if (LevelManager.Instance.levelIdx != 0)
        {
            tutorialHand.gameObject.SetActive(false);
        }
        else
        {
#if UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1
            tutorialHand.gameObject.SetActive(true);
            tutorialHand.DOMoveY(tutorialHand.transform.position.y + Screen.height * 0.1f, 1f).SetLoops(-1, LoopType.Restart);
#else
            tutorialHand.gameObject.SetActive(false);
#endif
        }

        SetMoneyText(MoneyManager.Instance.GetTotalCoin());
        SetMoveCountText(0);
    }

    void Update()
    {
        if (LevelManager.Instance.levelIdx != 0) return;

        if (Input.GetMouseButtonDown(0))
        {
            tutorialHand.DOScale(Vector3.zero, 0.2f).OnComplete(() =>
            {
                tutorialHand.gameObject.SetActive(false);
            });
        }
    }

    public void PlayMoneyFieldAnimation()
    {
        if (DOTween.IsTweening(moneyBG)) return;

        moneyBG.DOScale(Vector3.one * 1.2f, .1f).OnComplete(() =>
        {
            moneyBG.DOScale(Vector3.one, .1f);
        });
    }

    public void SetMoneyText(int val)
    {
        moneyText.text = val.ToString();
    }

    public void SetMoveCountText(int val)
    {
        moveCountText.text = "MOVE COUNT: " + val;
    }
}
