using UnityEngine;
using UnityEngine.UI;
using LostMyBuddy;

public class MainMenu : View
{
    [SerializeField] Button startButton, credtisButton, quitButton;

    public override void Init()
    {
        startButton.onClick.AddListener(() =>
        {
            if (!PlayerPrefs.HasKey("tutorial"))
            {
                UIManager.Instance.Show<HowToPlayScreen>();
                PlayerPrefs.SetInt("tutorial", 1);
            }
            else
            {
                UIManager.Instance.Show<LevelSelectMenu>();
            }
            UIManager.Instance.Hide<MainMenu>();
        });

        credtisButton.onClick.AddListener(() =>
        {
            UIManager.Instance.Show<HowToPlayScreen>();
            UIManager.Instance.Hide<MainMenu>();
        });

        quitButton.onClick.AddListener(() =>
        {
            GameManager.Instance.Quit();
        });
    }
}
