using UnityEngine;
using UnityEngine.UI;
using TMPro;
using LostMyBuddy;

public class LevelCompletedScreen : View
{
    [SerializeField] TMP_Text titleText;
    [SerializeField] Button nextButton, backToMenuButton;
    [SerializeField] TMP_Text moveCountText;
    [SerializeField] StarsUI starsUI;

    public override void Init()
    {
        nextButton.onClick.AddListener(() =>
        {
            Player.Instance.Restart();
            GridManager.Instance.Clear();
            GameManager.Instance.StartGame(LevelManager.Instance.currentPlayingLevel + 1);
        });

        backToMenuButton.onClick.AddListener(() =>
        {
            Player.Instance.Restart();
            GridManager.Instance.Clear();
            UIManager.Instance.Show<LevelSelectMenu>();
        });
    }

    public override void Show()
    {
        base.Show();

        if (LevelManager.Instance.IsLastLevel)
        {
            titleText.text = "THANKS FOR PLAYING";
            nextButton.gameObject.SetActive(false);
        }
        else
        {
            titleText.text = "LEVEL COMPLETE";
            nextButton.gameObject.SetActive(true);
        }
    }

    public void SetCountText(int count)
    {
        moveCountText.text = count + " moves";
        var list = LevelManager.Instance.GetLevel(0).starsMoveCount;
        var starCount = 0;

        for (int i = 0; i < list.Count; i++)
        {
            if (count < list[i]) starCount++;
            else break;
        }

        starsUI.ShowStarts(starCount);
        LevelManager.Instance.SetStarCount(LevelManager.Instance.currentPlayingLevel, starCount);
    }
}
