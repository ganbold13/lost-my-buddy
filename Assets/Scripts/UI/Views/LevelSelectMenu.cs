using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using LostMyBuddy;

public class LevelSelectMenu : View
{
    [SerializeField] LevelSelectButton levelSelectButton;
    [SerializeField] ScrollRect scrollRect;
    [SerializeField] Transform contentParent;
    [SerializeField] Transform cloudParent;
    [SerializeField] Button backButton;

    List<LevelSelectButton> levelSelectButtons = new();

    public override void Init()
    {
        int lvl = LevelManager.Instance.LevelCount;
        int currentLvl = LevelManager.Instance.levelIdx;

        for (int i = lvl - 1; i >= 0; i--)
        {
            var btn = Instantiate(levelSelectButton, contentParent);
            btn.Init(i, LevelManager.Instance.GetStarCount(i), i <= currentLvl);
            levelSelectButtons.Add(btn);
        }

        levelSelectButtons.Reverse();

        backButton.onClick.AddListener(() =>
        {
            UIManager.Instance.Show<MainMenu>();
        });
    }

    public void UpdateInfo()
    {
        for (int i = 0; i < levelSelectButtons.Count; i++)
        {
            levelSelectButtons[i].UpdateInfo(LevelManager.Instance.GetStarCount(i), i <= LevelManager.Instance.levelIdx);
        }
    }

    public override void Show()
    {
        base.Show();
        UpdateInfo();
        StartCoroutine(ForceScrollDown());
        cloudParent.DOScaleY(1.1f, 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
    }

    IEnumerator ForceScrollDown()
    {
        // Wait for end of frame AND force update all canvases before setting to bottom.
        yield return new WaitForEndOfFrame();
        Canvas.ForceUpdateCanvases();
        scrollRect.verticalNormalizedPosition = 0f;
        Canvas.ForceUpdateCanvases();
    }
}
