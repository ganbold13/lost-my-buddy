using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using LostMyBuddy;

public class LevelSelectButton : MonoBehaviour
{
    [SerializeField] Color activeColor, inactiveColor, activeShadowColor, inactiveShadowColor;

    [Space]
    [SerializeField] Button button;
    [SerializeField] StarsUI starsUI;
    [SerializeField] Image background;
    [SerializeField] Shadow shadow;
    [SerializeField] TMP_Text levelText;
    [SerializeField] GameObject lockedIcon;

    int levelIndex;

    public void Init(int level, int starCount, bool active)
    {
        levelIndex = level;
        starsUI.ShowStarts(starCount);
        levelText.text = (levelIndex + 1).ToString();

        button.onClick.AddListener(() =>
        {
            GameManager.Instance.StartGame(levelIndex);
            UIManager.Instance.Hide<LevelSelectMenu>();
        });

        if (active) Activate();
        else Deactivate();
    }

    public void UpdateInfo(int starCount, bool active)
    {
        starsUI.ShowStarts(starCount);

        if (active) Activate();
        else Deactivate();
    }

    public void Activate()
    {
        background.color = activeColor;
        shadow.effectColor = activeShadowColor;
        starsUI.gameObject.SetActive(true);
        levelText.gameObject.SetActive(true);
        lockedIcon.SetActive(false);
        button.interactable = true;
    }

    public void Deactivate()
    {
        background.color = inactiveColor;
        shadow.effectColor = inactiveShadowColor;
        starsUI.gameObject.SetActive(false);
        levelText.gameObject.SetActive(false);
        lockedIcon.SetActive(true);
        button.interactable = false;
    }
}
