using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class StarsUI : MonoBehaviour
{
    [SerializeField] List<Transform> starParents;
    [SerializeField] List<Image> images;

    public void ShowStarts(int count)
    {
        for (int i = 0; i < images.Count; i++)
        {
            images[i].color = new Color(1, 1, 1, 1);
        }

        for (int i = 0; i < count; i++)
        {
            ShowStar(i);
        }
    }

    void ShowStar(int idx)
    {
        if (idx >= starParents.Count) return;

        images[idx].DOColor(new Color(1, 1, 1, 0), 0.2f);
        starParents[idx].DOPunchScale(Vector3.one * 1.1f, 0.2f);
    }
}
