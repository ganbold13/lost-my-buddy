using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LostMyBuddy;

public class MovementCountHandler : MonoBehaviour
{
    int moveCount;

    public void IncreaseMoveCount()
    {
        moveCount++;
        UIManager.Instance.GetView<Hud>().SetMoveCountText(moveCount);
    }

    public void Clear()
    {
        moveCount = 0;
        UIManager.Instance.GetView<Hud>().SetMoveCountText(moveCount);
    }

    public int GetCount()
    {
        return moveCount;
    }
}
