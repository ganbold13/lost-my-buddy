using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wire : MonoBehaviour
{
    public int iterations = 5;
    public float gravity = 10;
    public float damping = 0.7f;
    public int numPoints = 20;
    public float meshThickness = 1;
    public MeshFilter meshFilter;
    public int cylinderResolution = 5;
    public bool Done;

    Vector3 startPoint, endPoint;
    Transform target;

    float pathLength;
    float pointSpacing;
    Vector3[] points;
    Vector3[] pointsOld;

    Mesh mesh;
    bool pinStart = true;
    bool pinEnd = true;
    bool IsInit;

    public void Init(Vector3 p0, Vector3 p1, Transform tf)
    {
        this.startPoint = p0;
        this.endPoint = p1;
        target = tf;

        points = new Vector3[numPoints];
        pointsOld = new Vector3[numPoints];

        for (int i = 0; i < numPoints; i++)
        {
            float t = i / (numPoints - 1f);
            points[i] = Vector3.Lerp(startPoint, endPoint, t);
            pointsOld[i] = points[i];
        }

        for (int i = 0; i < numPoints - 1; i++)
        {
            pathLength += Vector3.Distance(points[i], points[i + 1]);
        }
        pointSpacing = pathLength / points.Length;

        IsInit = true;
    }

    void LateUpdate()
    {
        if (!IsInit) return;

        CylinderGenerator.CreateMesh(ref mesh, points, cylinderResolution, meshThickness);
        meshFilter.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity); // mesh is in worldspace
        meshFilter.mesh = mesh;
    }


    void FixedUpdate()
    {
        if (!IsInit) return;

        points[0] = startPoint;
        for (int i = 0; i < points.Length; i++)
        {
            bool pinned = (i == 0 && pinStart) || (i == points.Length - 1 && pinEnd);
            if (!pinned)
            {
                Vector3 curr = points[i];
                points[i] = points[i] + (points[i] - pointsOld[i]) * damping + Vector3.down * gravity * Time.deltaTime * Time.deltaTime;
                pointsOld[i] = curr;
            }
        }

        if (!Done)
            points[^1] = target.position;

        for (int i = 0; i < iterations; i++)
        {
            ConstrainCollisions();
            ConstrainConnections();
        }
    }

    void ConstrainConnections()
    {
        for (int i = 0; i < points.Length - 1; i++)
        {
            Vector3 centre = (points[i] + points[i + 1]) / 2;
            Vector3 offset = points[i] - points[i + 1];
            float length = offset.magnitude;
            Vector3 dir = offset / length;

            //if (length > pointSpacing || length < pointSpacing * 0.5f)
            {
                //float desiredLength = Mathf.Min(length, pointSpacing);
                //desiredLength = Mathf.Lerp(desiredLength, pointSpacing, 0.25f * Time.deltaTime);
                if (i != 0 || !pinStart)
                {
                    points[i] = centre + dir * pointSpacing / 2;
                }
                if (i + 1 != points.Length - 1 || !pinEnd)
                {
                    points[i + 1] = centre - dir * pointSpacing / 2;
                }
            }
        }
    }

    void ConstrainCollisions()
    {

        for (int i = 0; i < points.Length; i++)
        {
            bool pinned = i == 0 || i == points.Length - 1;
            if (!pinned)
            {
                if (points[i].y < meshThickness / 2)
                {
                    points[i].y = meshThickness / 2;
                }
            }
        }
    }

    void OnDrawGizmos()
    {
        if (points != null)
        {
            for (int i = 0; i < points.Length; i++)
            {
                Gizmos.DrawSphere(points[i], 0.05f);
            }
        }
    }
}
