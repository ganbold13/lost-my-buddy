using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using LostMyBuddy;

public class GridMovement : MonoBehaviour
{
    const float jumpDuration = 0.4f;

    [HideInInspector] public GridTile gridTile;
    readonly Stack<GridTile> pastMovements = new();

    Player player;
    GridTile startTile;

    bool IsMoving;
    Vector3 lastMousePosition;

    void Awake()
    {
        player = GetComponent<Player>();
    }

    void Update()
    {
        if (!GameManager.Instance.IsPlaying) return;
        if (IsMoving) return;
        if (player.HasGetBody) return;
        if (DOTween.IsTweening(transform)) return;

        Vector3 dir = Vector3.zero;

#if UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1
        if (Input.GetMouseButtonDown(0))
        {
            mp = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (mp == Vector3.zero) return;

            if (Mathf.Abs((Input.mousePosition - mp).y) > Mathf.Abs((Input.mousePosition - mp).x))
            {
                if ((Input.mousePosition - mp).y > 0)
                {
                    dir = Vector3.forward;
                }
                if ((Input.mousePosition - mp).y < 0)
                {
                    dir = Vector3.back;
                }
            }
            else
            {
                if ((Input.mousePosition - mp).x > 0)
                {
                    dir = Vector3.right;
                }
                if ((Input.mousePosition - mp).x < 0)
                {
                    dir = Vector3.left;
                }
            }

            mp = Vector3.zero;
        }

#else
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            dir = Vector3.forward;
        }

        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            dir = Vector3.back;
        }

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            dir = Vector3.left;
        }

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            dir = Vector3.right;
        }
#endif
        if (dir == Vector3.zero) return;

        if (!CanMove(dir))
        {
            transform.DOPunchPosition(dir * 0.1f, 0.2f);
            CameraManager.Instance.Shake(0.1f, 0.5f, 2.5f);
            return;
        }

        IsMoving = true;
        player.MovementCountHandler.IncreaseMoveCount();

        if (CheckHasBody(dir))
        {
            var rot1 = Quaternion.LookRotation(dir);

            transform.DORotate(rot1.eulerAngles, 0.2f);
            player.animationHandler.Float();
            transform.DOJump(transform.position + dir, 0.5f, 1, jumpDuration).OnComplete(() =>
           {
               gridTile = GridManager.Instance.grid.GetValue(transform.position);
               pastMovements.Push(gridTile);

               player.SpawnWire();
               player.ReachedBody();

               MoveBack();
               IsMoving = false;
           });

            return;
        }

        player.animationHandler.Jump();

        var rot = Quaternion.LookRotation(dir);

        transform.DORotate(rot.eulerAngles, 0.2f);
        transform.DOJump(transform.position + dir, 0.5f, 1, jumpDuration).OnComplete(() =>
        {
            gridTile = GridManager.Instance.grid.GetValue(transform.position);
            pastMovements.Push(gridTile);

            player.SpawnWire();

            IsMoving = false;
        });
    }

    public void Init()
    {
        gridTile = GridManager.Instance.grid.GetValue(transform.position);
        startTile = gridTile;
        pastMovements.Push(gridTile);
    }

    public void ClearMovements()
    {
        pastMovements.Clear();
    }

    bool CanMove(Vector3 direction)
    {
        GridTile targetTile = GridManager.Instance.grid.GetValue(transform.position + direction);
        return targetTile != default && targetTile.available;
    }

    bool CheckHasBody(Vector3 direction)
    {
        GridTile targetTile = GridManager.Instance.grid.GetValue(transform.position + direction);
        return targetTile != default && targetTile.Body != null;
    }

    bool CheckBoxes(Vector3 direction)
    {
        if (GridManager.Instance.grid.GetValue(transform.position + direction).CurrentBox != null)
        {
            if (GridManager.Instance.grid.GetValue(transform.position + direction).CurrentBox.CanMove(direction))
            {
                GridManager.Instance.grid.GetValue(transform.position + direction).CurrentBox.MoveTo(direction);
                return true;
            }
            else return false;
        }

        return true;
    }

    private void MoveBack()
    {
        if (!GameManager.Instance.IsPlaying) return;

        if (pastMovements.Count > 0)
        {
            var tmp = pastMovements.Peek();
            Wire wirePrev = player.wires.Peek();
            wirePrev.Done = false;

            if (CheckBoxes(new Vector3(tmp.transform.position.x, transform.position.y, tmp.transform.position.z) - transform.position))
            {
                var rot = Quaternion.LookRotation(-new Vector3(tmp.transform.position.x, transform.position.y, transform.position.z) + transform.position);

                transform.DORotate(rot.eulerAngles, 0.2f);
                transform.DOMove(new Vector3(tmp.transform.position.x, transform.position.y, tmp.transform.position.z), 0.35f).OnComplete(() =>
                {
                    Destroy(wirePrev.gameObject);
                    gridTile = pastMovements.Pop();
                    player.wires.Pop();

                    if (gridTile == startTile)
                    {
                        player.Win();
                        GameManager.Instance.LevelComplete();
                    }

                    MoveBack();
                }).SetEase(Ease.Linear);
            }
            else
            {
                transform.DOPunchPosition((new Vector3(tmp.transform.position.x, transform.position.y, tmp.transform.position.z) - transform.position) * 0.1f, 0.2f);

                player.CannotMoveBack();
            }
        }
    }
}
