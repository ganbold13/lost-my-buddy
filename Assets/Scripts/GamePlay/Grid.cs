using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LostMyBuddy
{
    public class Grid<T>
    {
        public int width { get; private set; }
        public int height { get; private set; }
        T[,] gridArray;
        Vector3 origin;
        float cellSize;
        public Grid(int width, int height, Vector3 origin, float cellSize)
        {
            this.width = width;
            this.height = height;
            this.cellSize = cellSize;
            this.origin = origin;
            gridArray = new T[width, height];
        }

        public void GetXY(Vector3 worldPos, out int x, out int y)
        {
            x = Mathf.RoundToInt((worldPos - origin).x / cellSize);
            y = Mathf.RoundToInt((worldPos - origin).z / cellSize);
        }
        public Vector3 GetWorldPos(int x, int y)
        {
            return new Vector3((origin.x + x) * cellSize, origin.y, (origin.z + y) * cellSize);
        }

        public void SetValue(int x, int y, T value)
        {
            if (x >= 0 && y >= 0 && x < width && y < height)
            {
                gridArray[x, y] = value;
            }
        }

        public void SetValue(Vector3 worldPos, T value)
        {
            int x, y;
            GetXY(worldPos, out x, out y);
            SetValue(x, y, value);
        }

        public T GetValue(int x, int y)
        {
            if (x >= 0 && y >= 0 && x < width && y < height)
            {
                return gridArray[x, y];
            }
            return default;
        }

        public T GetValue(Vector3 worldPos)
        {
            int x, y;
            GetXY(worldPos, out x, out y);
            return GetValue(x, y);
        }

        public Vector3 GetWorldPos(Vector3 position)
        {
            GetXY(position, out int x, out int y);
            return GetWorldPos(x, y);
        }
    }
}
