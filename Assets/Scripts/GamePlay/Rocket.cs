using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    float posY;
    void Start()
    {
        posY = transform.position.y;
        transform.DOMoveY(posY + .25f, 1f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
    }
}
