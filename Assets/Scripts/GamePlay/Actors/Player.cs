using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace LostMyBuddy
{
    public class Player : Singleton<Player>
    {
        public System.Action OnGoalReached;

        public Stack<Wire> wires = new();
        public CharacterAnimationHandler animationHandler;
        public MovementCountHandler MovementCountHandler;

        public bool HasGetBody;

        [HideInInspector] public GridMovement GridMovementHandler;

        [SerializeField] GameObject model;

        [Space, Header("Particles")]
        [SerializeField] ParticleSystem PS_dust;
        [SerializeField] ParticleSystem PS_death;

        [Space, Header("Wire Related")]
        [SerializeField] Transform wirePinnedPoint;
        [SerializeField] Wire wire;
        [SerializeField] Transform rocket;

        public Rigidbody Rigidbody => rb;

        Wire currentWire;
        Collider col;
        Rigidbody rb;
        Vector3 modelPos;

        void Awake()
        {
            col = GetComponent<Collider>();
            rb = GetComponent<Rigidbody>();
            MovementCountHandler = GetComponent<MovementCountHandler>();
            GridMovementHandler = GetComponent<GridMovement>();
            modelPos = model.transform.localPosition;
        }

        public void SetupWire()
        {
            currentWire = InstantiateWire(rocket.position, model.transform.position, model.transform);
            currentWire.Done = true;

            currentWire = InstantiateWire(model.transform.position, wirePinnedPoint.position, model.transform);

            GridMovementHandler.Init();
        }

        public void SpawnWire()
        {
            currentWire.Done = true;
            currentWire = InstantiateWire(model.transform.position, wirePinnedPoint.position, model.transform);
        }

        Wire InstantiateWire(Vector3 startPosition, Vector3 endPosition, Transform parent)
        {
            var newWire = Instantiate(wire);
            wires.Push(newWire);
            newWire.Init(startPosition, endPosition, parent);
            return newWire;
        }

        public void Restart()
        {
            gameObject.SetActive(true);
            HasGetBody = false;
            col.isTrigger = true;
            rb.isKinematic = true;
            Goal.Instance.transform.SetParent(null);
            PS_death.transform.SetParent(transform);
            PS_death.transform.localPosition = Vector3.zero;

            while (wires.Count > 0)
            {
                var wire = wires.Pop();
                Destroy(wire.gameObject);
            }

            model.transform.localPosition = modelPos;
            animationHandler.Idle();
            GridMovementHandler.ClearMovements();
            GetComponent<MovementCountHandler>().Clear();
        }

        public void ReachedBody()
        {
            col.isTrigger = false;
            rb.isKinematic = false;
            HasGetBody = true;

            OnGoalReached?.Invoke();
        }

        public void Win()
        {
            rb.isKinematic = true;
            animationHandler.Victory();
            var moveCount = GetComponent<MovementCountHandler>().GetCount();
            UIManager.Instance.GetView<LevelCompletedScreen>().SetCountText(moveCount);

            if (LevelManager.Instance.IsLastLevel)
            {
                Goal.Instance.Win();
            }
        }

        void OnCollisionEnter(Collision col)
        {
            if (col.gameObject.CompareTag(Tags.Obstacle))
            {
                Death();
            }
        }

        public void Death()
        {
            transform.DOKill();
            PS_death.transform.SetParent(null);
            gameObject.SetActive(false);
            PS_death.Play();
            CameraManager.Instance.Shake();
            GameManager.Instance.GameOver();
        }

        public void CannotMoveBack()
        {
            PS_dust.Stop();
            CameraManager.Instance.Shake();
            GameManager.Instance.GameOver();
        }
    }
}