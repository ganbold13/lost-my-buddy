using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using LostMyBuddy;

public class Collectable : MonoBehaviour
{
    [SerializeField] Transform model;
    [SerializeField] ParticleSystem PS_collect;
    [SerializeField] float rotateDuration = .1f, bounceDuration = 0.5f;
    void Start()
    {
        model.DORotate(new Vector3(0, 15, 0), rotateDuration).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        model.DOLocalMoveY(model.localPosition.y + 0.5f, bounceDuration).SetLoops(-1, LoopType.Yoyo);
    }

    void Collect()
    {
        MoneyManager.Instance.IncreaseMoney();
        model.DOKill();
        PS_collect.transform.SetParent(null);
        PS_collect.Play();
        Destroy(gameObject);
        Destroy(PS_collect.gameObject, 1.1f);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            // if (other.GetComponent<Player>().HasGetBody)
            // {
            Collect();
            // }
        }
    }
}
