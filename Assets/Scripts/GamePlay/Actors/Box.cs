using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using LostMyBuddy;

public class Box : MonoBehaviour, IActivatable
{
    [SerializeField] Transform activateParent;

    void Start()
    {
        GridManager.Instance.grid.GetValue(transform.position).CurrentBox = this;
        activateParent.gameObject.SetActive(false);
        Player.Instance.OnGoalReached += Activate;
    }

    void OnDestroy()
    {
        Player.Instance.OnGoalReached -= Activate;
    }

    public bool CanMove(Vector3 dir)
    {
        var grid = GridManager.Instance.grid.GetValue(transform.position + dir);
        if (grid != default)
            if (grid.available)
            {
                if (grid.CurrentBox == null)
                    return true;
                else
                {
                    if (grid.CurrentBox.CanMove(grid.CurrentBox.transform.position + dir))
                        return true;
                }
            }
        return false;
    }

    public void MoveTo(Vector3 dir)
    {
        GridManager.Instance.grid.GetValue(transform.position).CurrentBox = null;
        transform.DOMove(transform.position + dir, 0.345f).OnComplete(() =>
        {
            GridManager.Instance.grid.GetValue(transform.position).CurrentBox = this;
        }).SetEase(Ease.Linear);
    }

    public void Activate()
    {
        activateParent.gameObject.SetActive(true);
        activateParent.DOLocalMoveY(0, 0.5f);
    }
}
