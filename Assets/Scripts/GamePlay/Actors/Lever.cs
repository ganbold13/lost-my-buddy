using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using LostMyBuddy;

public class Lever : MonoBehaviour
{
    [SerializeField] Transform model;

    System.Action OnLevered;

    List<GameObject> obstacles = new();

    void Start()
    {
        obstacles = new(GameObject.FindGameObjectsWithTag(Tags.Obstacle));

        foreach (GameObject go in obstacles)
        {
            OnLevered += () =>
            {
                if (go != null)
                {
                    go.transform.DOMoveY(-1, 1);
                    go.transform.DOScaleY(0, 0.5f).OnComplete(() =>
                    {
                        go.SetActive(false);
                    });
                }
            };
        }
    }

    public void Levered()
    {
        model.transform.DORotate(new Vector3(0, 0, -40), 0.25f).SetEase(Ease.Linear).OnComplete(() =>
        {
            OnLevered?.Invoke();
        });
        GetComponent<Collider>().enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.Player))
        {
            if (other.GetComponent<Player>().HasGetBody)
                Levered();
        }
    }
}
