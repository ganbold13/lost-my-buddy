using UnityEngine;
using DG.Tweening;
using LostMyBuddy;

public class Obstacle : MonoBehaviour, IActivatable
{
    [SerializeField] Transform activateParent;

    public void Activate()
    {
        activateParent.gameObject.SetActive(true);
        activateParent.DOLocalMoveY(0, 0.5f);
    }

    void Start()
    {
        activateParent.gameObject.SetActive(false);
        Player.Instance.OnGoalReached += Activate;
    }

    void OnDestroy()
    {
        Player.Instance.OnGoalReached -= Activate;
    }
}
