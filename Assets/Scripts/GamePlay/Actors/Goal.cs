using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using LostMyBuddy;

public class Goal : Singleton<Goal>
{
    [SerializeField] Transform buddyModel, keyModel;
    [SerializeField] BuddyMsgHandler message;

    Transform model;

    public void Start()
    {
        Init();
        Player.Instance.OnGoalReached += Activate;
    }

    public void Init()
    {
        buddyModel.gameObject.SetActive(false);
        keyModel.gameObject.SetActive(false);
        message.gameObject.SetActive(false);

        if (LevelManager.Instance.IsLastLevel)
        {
            model = buddyModel;
            message.gameObject.SetActive(true);
            message.Init();
        }
        else
        {
            model = keyModel;
        }

        model.gameObject.SetActive(true);
        PlayAnimation();
    }

    public void Win()
    {
        buddyModel.GetComponentInChildren<Animator>().SetTrigger("Victory");
    }

    void PlayAnimation()
    {
        model.DOKill();

        var localRot = model.localRotation;

        model.DOLocalRotate(new Vector3(localRot.x, localRot.y + 10, localRot.z), 0.5f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Incremental);
        model.DOLocalMoveY(0.25f, 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
    }

    void Activate()
    {
        model.DOKill();
        transform.SetParent(Player.Instance.transform);
        model.DOLocalRotate(Vector3.zero, 0.25f);
        model.DOLocalMove(new Vector3(0, -.25f, 0.1f), 0.25f);
        if (model == buddyModel) message.ActivateEmoji();
    }
}
