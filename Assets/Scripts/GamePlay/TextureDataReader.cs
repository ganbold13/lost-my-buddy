using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureDataReader : Singleton<TextureDataReader>
{
    public Texture2D texture;

    public int Width => texture.width;
    public int Height => texture.height;

    public void Init()
    {

    }

    // Make sure that texture is:
    // - read/write is true
    // - generate mip maps is false
    // - make Format to RGB
    public Color GetPixelColor(int x, int y)
    {
        return texture.GetPixel(x, y);
    }
}

public static class TextureDataColors
{
    public static Color UNAVIABLE = Color.black;
    public static Color AVIABLE = Color.white;
    public static Color OBSTACLE = Color.red;
    public static Color BODY = Color.green;
    public static Color BOX = Color.blue;
    public static Color SOUL = Color.magenta;
    public static Color LEVER = new(1, 1, 0);
}
