using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UIElements;

public class TextBasedDataReader : Singleton<TextBasedDataReader>
{
    List<string> currentLevelData;

    public void Init(int levelIdx)
    {
        var currentLevel = LevelManager.Instance.GetLevel(levelIdx);
        currentLevelData = new(currentLevel.data);
        currentLevelData.Reverse();
    }

    public char GetChar(int col, int row)
    {
        return currentLevelData[row][col];
    }

    public int GetRow()
    {
        return currentLevelData.Count;
    }

    public int GetColumn(int row)
    {
        return currentLevelData[row].Length;
    }
}

public static class CharacterData
{
    public static char UNAVIABLE = '0';
    public static char AVIABLE = '1';
    public static char OBSTACLE = 'x';
    public static char PLAYER = 'p';
    public static char GOAL = 'g';
    public static char BOX = 'b';
    public static char LEVER = 'l';
    public static char COIN = 'c';
}