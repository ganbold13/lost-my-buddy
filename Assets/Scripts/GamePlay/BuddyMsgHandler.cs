using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BuddyMsgHandler : MonoBehaviour
{
    [SerializeField] GameObject helpText;
    [SerializeField] Transform emojisParent;
    List<Transform> emojis;

    void Start()
    {
        Init();
    }

    public void Init()
    {
        transform.DOKill();

        emojis = new(emojisParent.GetComponentsInChildren<Transform>());
        emojis.RemoveAt(0);
        helpText.SetActive(true);

        for (int i = 0; i < emojis.Count; i++)
        {
            emojis[i].gameObject.SetActive(false);
        }

        emojis[Random.Range(0, emojis.Count)].gameObject.SetActive(true);

        emojisParent.gameObject.SetActive(false);

        transform.DOScale(Vector3.one * 0.35f, 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        transform.DOLocalRotate(new Vector3(27, 0, 5), 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
    }

    public void ActivateEmoji()
    {
        transform.DOKill();

        transform.DOLocalRotate(new Vector3(27, 0, 0), 0.1f);
        transform.DOScale(Vector3.one * 0.5f, 0.5f).SetEase(Ease.OutBack).OnComplete(() =>
        {
            transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.Linear);
        });

        helpText.SetActive(false);
        emojisParent.gameObject.SetActive(true);
    }
}
