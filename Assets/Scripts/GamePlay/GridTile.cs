using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTile : MonoBehaviour
{
    public bool available = true;
    public Box CurrentBox;
    public Transform Body;

    void Start()
    {
        if (!available) gameObject.SetActive(false);
    }
}
