using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LostMyBuddy
{
    public class MoneyManager : Singleton<MoneyManager>
    {
        int totalCoin;

        public void IncreaseMoney(int val = 1)
        {
            totalCoin += val;
            UIManager.Instance.GetView<Hud>().SetMoneyText(totalCoin);
            UIManager.Instance.GetView<Hud>().PlayMoneyFieldAnimation();
        }

        public int GetTotalCoin(){
            return totalCoin;
        }

        public  void SetTotalCoin(int count){
            totalCoin = count;
        }
    }
}