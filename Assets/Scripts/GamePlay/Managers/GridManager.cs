using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LostMyBuddy;

public class GridManager : Singleton<GridManager>
{
    public Grid<GridTile> grid;
    [SerializeField] GridTile gridTilePf;
    [SerializeField] Box boxPf;
    [SerializeField] Transform goal, player, rocket;
    [SerializeField] GameObject obstaclePf, leverPf;
    [SerializeField] GameObject coinPf;

    List<GameObject> spawnedObjects = new();

    int row;
    int column;

    public void Clear()
    {
        grid = null;
        for (int i = 0; i < spawnedObjects.Count; i++)
        {
            Destroy(spawnedObjects[i]);
        }
    }

    public void Init()
    {
        row = TextBasedDataReader.Instance.GetRow();
        column = TextBasedDataReader.Instance.GetColumn(0);

        grid = new Grid<GridTile>(row, column, transform.position, 1f);

        for (int i = row - 1; i >= 0; i--)
        {

            int col = TextBasedDataReader.Instance.GetColumn(i);

            for (int j = 0; j < col; j++)
            {
                char c = TextBasedDataReader.Instance.GetChar(j, i);
                var pos = grid.GetWorldPos(j, i);
                var gridClone = Instantiate(gridTilePf, pos, Quaternion.identity);
                spawnedObjects.Add(gridClone.gameObject);

                if (c == CharacterData.LEVER)
                {
                    var obj = Instantiate(leverPf, pos + Vector3.up, Quaternion.identity);
                    spawnedObjects.Add(obj);
                }

                if (c == CharacterData.UNAVIABLE)
                {
                    gridClone.available = false;
                }

                if (c == CharacterData.BOX)
                {
                    var obj = Instantiate(boxPf, pos + Vector3.up, Quaternion.identity);
                    spawnedObjects.Add(obj.gameObject);
                }

                if (c == CharacterData.OBSTACLE)
                {
                    var obj = Instantiate(obstaclePf, pos + Vector3.up, Quaternion.identity);
                    spawnedObjects.Add(obj);
                }

                if (c == CharacterData.COIN)
                {
                    var obj = Instantiate(coinPf, pos + Vector3.up * 0.5f, Quaternion.identity);
                    spawnedObjects.Add(obj);
                }

                if (c == CharacterData.GOAL)
                {
                    goal.transform.position = pos + Vector3.up;
                    gridClone.Body = goal.transform;
                }

                if (c == CharacterData.PLAYER)
                {
                    player.transform.position = pos + Vector3.up;
                    rocket.transform.position = pos + Vector3.back;
                }

                grid.SetValue(j, i, gridClone);
            }
        }

        Player.Instance.SetupWire();
    }
}
