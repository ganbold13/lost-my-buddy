using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraManager : Singleton<CameraManager>
{
    [SerializeField] CinemachineVirtualCamera virtualCamera;

    CinemachineBasicMultiChannelPerlin cinemachineBasicMultiChannelPerlin;

    void Start()
    {
        cinemachineBasicMultiChannelPerlin = virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

#if UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1
        virtualCamera.m_Lens.FieldOfView = 40;
#else
        virtualCamera.m_Lens.FieldOfView = 20;
#endif
    }

    public void Shake(float duration = 0.2f, float intensity = 1, float frequency = 5)
    {
        StartCoroutine(Shake_Co(duration, intensity, frequency));
    }

    IEnumerator Shake_Co(float duration, float intensity, float frequency)
    {
        float timer = 0;
        while (timer < duration)
        {
            timer += Time.deltaTime;
            cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = intensity;
            cinemachineBasicMultiChannelPerlin.m_FrequencyGain = frequency;
            yield return null;
        }

        cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = 0;
    }
}
