using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LostMyBuddy
{
    public class UIManager : Singleton<UIManager>
    {
        List<View> _views;
        View _currentView;
        private readonly Stack<View> _history = new();

        void Start()
        {
            _views = FindObjectsOfType<View>(true).ToList();
            Init();
        }

        void Init()
        {
            foreach (var view in _views)
            {
                view.Hide();
                view.Init();
            }
            _history.Clear();
            Show<MainMenu>();
            GameManager.Instance.OnGameStateChange += (GameState state) =>
            {
                if (state == GameState.Playing)
                {
                    Show<Hud>();
                    Hide<PauseMenu>();
                    Hide<LevelCompletedScreen>();
                    Hide<GameOverScreen>();
                }
                if (state == GameState.GameOver)
                {
                    Hide<Hud>();
                    Show<GameOverScreen>();
                }
                if (state == GameState.LevelCompleted)
                {
                    Hide<Hud>();
                    Show<LevelCompletedScreen>();
                }
                if (state == GameState.Paused)
                {
                    Hide<Hud>();
                    Show<PauseMenu>();
                }
            };
        }

        /// <summary>
        /// View-ийг Type-аар нь хайж олох
        /// </summary>
        /// <typeparam name="T">View ээс удамшсан Type</typeparam>
        public T GetView<T>() where T : View
        {
            foreach (var view in _views)
            {
                if (view is T tView)
                {
                    return tView;
                }
            }

            return null;
        }

        /// <summary>
        /// View-ийг Type-аар нь хайж Active болгох
        /// </summary>
        /// <param name="keepInHistory">Хуудасны history stack дээр нэмэх</param>
        /// <typeparam name="T">View ээс удамшсан Type</typeparam>
        public void Show<T>(bool keepInHistory = true) where T : View
        {
            foreach (var view in _views)
            {
                if (view is T)
                {
                    Show(view, keepInHistory);
                    break;
                }
            }
        }

        /// <summary>
        /// View-ийг Type-аар нь хайж Active болгох
        /// </summary>
        /// <param name="keepInHistory">Хуудасны history stack дээр нэмэх</param>
        /// <typeparam name="T">View ээс удамшсан Type</typeparam>
        public void Hide<T>() where T : View
        {
            foreach (var view in _views)
            {
                if (view is T)
                {
                    Hide(view);
                    break;
                }
            }
        }

        /// <summary>
        /// View-ийг харуулж бусад View-ийг нуух
        /// </summary>
        /// <param name="keepInHistory">Хуудасны history stack дээр нэмэх</param>
        public void Show(View view, bool keepInHistory)
        {
            if (_history.Count == 0) _history.Push(view);

            if (_currentView != null)
            {
                if (keepInHistory)
                {
                    _history.Push(_currentView);
                }

                _currentView.Hide();
            }
            view.Show();
            _currentView = view;
        }


        /// <summary>
        /// View-ийг харуулж бусад View-ийг нуух
        /// </summary>
        /// <param name="keepInHistory">Хуудасны history stack дээр нэмэх</param>
        public void Hide(View view)
        {
            if (_history.Count > 0)
            {
                _history.Pop();
            }

            view.Hide();
            _currentView = null;
        }

        /// <summary>
        /// Хуучин View руу очих
        /// </summary>
        public void GoBack()
        {
            if (_history.Count != 0)
            {
                Show(_history.Pop(), false);
            }
        }
    }
}