using System.Collections;
using System.Collections.Generic;
using LostMyBuddy;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    public int LevelCount => levels.Count;
    public bool IsLastLevel => currentPlayingLevel >= LevelCount - 1;

    public int levelIdx;
    public int currentPlayingLevel;

    [SerializeField] List<LevelData> levels;
    List<int> starCounts = new List<int>();

    void Awake()
    {
        GameManager.Instance.OnGameStateChange += ((GameState state) =>
        {
            if (state == GameState.LevelCompleted)
            {
                if (currentPlayingLevel >= levelIdx) levelIdx = currentPlayingLevel + 1;

                GameManager.Instance.SaveData();
            }
        });

        for (int i = 0; i < LevelCount; i++) starCounts.Add(0);

        LoadData();
    }

    public LevelData GetLevel(int idx)
    {
        if (levels == null || levels.Count == 0) return null;

        return levels[idx];
    }

    public void SetStarCount(int lvlIdx, int count)
    {
        starCounts[lvlIdx] = count;
        SaveData();
    }

    public int GetStarCount(int idx)
    {
        return starCounts[idx];
    }

    void LoadData()
    {
        var data = SaveManager.Load<Data>();

        if (data.progresses.Count == 0)
        {
            for (int i = 0; i < starCounts.Count; i++)
            {
                data.progresses.Add(new LevelProgress(i, starCounts[i]));
            }
        }

        for (int i = 0; i < data.progresses.Count; i++)
        {
            var progresses = data.progresses[i];
            starCounts[progresses.levelIdx] = progresses.starCount;
        }

        SaveManager.Save(data);
    }

    public void SaveData()
    {
        var data = SaveManager.Load<Data>();
        for (int i = 0; i < starCounts.Count; i++)
        {
            data.progresses[i].levelIdx = i;
            data.progresses[i].starCount = starCounts[i];
        }

        SaveManager.Save(data);
    }
}

[System.Serializable]
public class Data
{
    public List<LevelProgress> progresses;

    public Data()
    {
        progresses = new();
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}

[System.Serializable]
public class LevelProgress
{
    public int levelIdx;
    public int starCount;

    public LevelProgress(int levelIdx, int starCount)
    {
        this.levelIdx = levelIdx;
        this.starCount = starCount;
    }
}

