using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LostMyBuddy
{
    public enum GameState
    {
        Loading,
        Starting,
        Playing,
        GameOver,
        LevelCompleted,
        Paused,
        Reloading,
    }

    //Responsibilty:Changing GameState 
    public class GameManager : Singleton<GameManager>
    {
        public event System.Action<GameState> OnGameStateChange;
        private GameState _state = GameState.Starting;
        public GameState State
        {
            get => _state;
            private set
            {
                _state = value;
                OnGameStateChange?.Invoke(_state);
            }
        }

        void Awake()
        {
            LoadData();
        }

        public bool IsPlaying => State == GameState.Playing;
        public void StartGame(int level)
        {
            State = GameState.Playing;
            TextBasedDataReader.Instance.Init(level);
            GridManager.Instance.Init();
            LevelManager.Instance.currentPlayingLevel = level;
            Goal.Instance.Init();
        }

        public void GameOver() => State = GameState.GameOver;
        public void LevelComplete() => State = GameState.LevelCompleted;
        public void Pause() => State = GameState.Paused;
        public void Resume() => State = GameState.Playing;
        public void Reload() => State = GameState.Reloading;
        public void Replay()
        {
            Player.Instance.Restart();
            GridManager.Instance.Clear();
            GridManager.Instance.Init();
            Goal.Instance.Init();
            Resume();
        }
        public void Quit() => Application.Quit();
        public void SaveData()
        {
            PlayerPrefs.SetInt("LevelIndex", LevelManager.Instance.levelIdx);
            PlayerPrefs.SetInt("TotalCoin", MoneyManager.Instance.GetTotalCoin());
        }

        public void LoadData()
        {
            LevelManager.Instance.levelIdx = PlayerPrefs.GetInt("LevelIndex", 0);
            MoneyManager.Instance.SetTotalCoin(PlayerPrefs.GetInt("TotalCoin", 0));
        }

        void OnApplicationQuit()
        {
            SaveData();
            LevelManager.Instance.SaveData();
        }
    }
}
