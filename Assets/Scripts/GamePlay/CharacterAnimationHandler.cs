using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationHandler : MonoBehaviour
{
    [SerializeField] Animator animator;

    public void Jump()
    {
        animator.SetTrigger("Jump");
    }

    public void Float()
    {
        animator.CrossFade("Float", 0.05f);
    }

    public void Victory()
    {
        animator.CrossFade("Victory", 0.05f);
    }

    public void Idle()
    {
        animator.CrossFade("Idle", 0.05f);
    }
}
