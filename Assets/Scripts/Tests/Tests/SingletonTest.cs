using UnityEngine;
using NUnit.Framework;

public class SingletonTests
{
    public class TestComponent : Singleton<TestComponent> {}

    [Test]
    public void Singleton_Instance_ReturnsSameInstance()
    {
        GameObject testObject = new GameObject();
        TestComponent testComponent = testObject.AddComponent<TestComponent>();
        TestComponent singletonInstance = Singleton<TestComponent>.Instance;
        Assert.AreEqual(testComponent, singletonInstance);
    }

    [Test]
    public void Singleton_Instance_IsNotNull()
    {
        TestComponent singletonInstance = Singleton<TestComponent>.Instance;
        Assert.IsNotNull(singletonInstance);
    }

    [Test]
    public void Singleton_DontDestroyOnLoad()
    {
        TestComponent singletonInstance = Singleton<TestComponent>.Instance;
        Assert.IsTrue(singletonInstance.gameObject.scene.isLoaded);
    }
}
