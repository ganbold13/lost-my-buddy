using UnityEngine;
using System.IO;

public static class Tools
{
    public static void Screenshot(string path, string name)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        string fileName = path + "/" + name + ".png";
#if UNITY_5
        Application.CaptureScreenshot (fileName, 1);
#else
        ScreenCapture.CaptureScreenshot(fileName, 1);
#endif
    }
}
