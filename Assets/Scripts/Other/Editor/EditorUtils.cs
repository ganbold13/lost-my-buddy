using UnityEditor;

public static class EditorUtils
{
    [MenuItem("Utils/Screenshot")]
    public static void Screenshot()
    {
        Tools.Screenshot("Screenshots/", "Screenshot" + System.DateTime.Now.ToString("_yyyy-MM-dd_hh-mm-ss"));
    }
}
