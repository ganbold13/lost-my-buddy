using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/LevelData")]
public class LevelData : ScriptableObject
{
    public List<string> data;
    public List<int> starsMoveCount = new(){13, 10, 7};
}
